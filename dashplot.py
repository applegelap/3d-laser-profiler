'''
Dash plotly test.
'''

import json
import pandas as pd
import numpy as np

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import plotly.graph_objects as go
import plotly.io as pio
from dash.exceptions import PreventUpdate

external_stylesheets = [{
    'href': './all.css',
    'rel': 'stylesheet',
    'integrity': 'sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf',
    'crossorigin': 'anonymous'
}]

plotly_template = pio.templates["plotly_dark"]
# print (plotly_template)

pio.templates["plotly_dark_custom"] = pio.templates["plotly_dark"]

# set the paper_bgcolor and the plot_bgcolor to a new color
pio.templates["plotly_dark_custom"]['layout']['paper_bgcolor'] = '#000000'
pio.templates["plotly_dark_custom"]['layout']['autosize'] = True


app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

styles = {
	'pre': {
		'overflowX': 'scroll',
		'overflowY': 'scroll',
		'height':100,
		'color':'white',
		'backgroundColor':'black',
		'border-style': None
	}
}
name='bladecrack_4'
# name2 = 'bladecrack_4'
input_file = "./csv_folder/" + name + ".csv"
# input_file2 = "../csv_folder/" + name2 + ".csv"
Z_DATA = pd.read_csv(input_file)
# Z_DATA2 = pd.read_csv(input_file2)


Z_DATA = Z_DATA.replace(-99.9999, np.NaN)
# Z_DATA2 = Z_DATA2.replace(-99.9999, np.NaN)
Z_DATA = Z_DATA.replace(-99.9997, np.NaN)
# Z_DATA2 = Z_DATA2.replace(-99.9997, np.NaN)

Z_DATA = np.asarray(Z_DATA, dtype=np.float32)

Z_DATA = np.where(Z_DATA == np.nan, np.nan, Z_DATA)



FIG = go.FigureWidget([go.Surface(z=Z_DATA, opacity=0.9,  colorscale='blues')])

FIG.update_layout(
	height=850,
	scene=dict(zaxis=dict(range=[-100, 100]),
				xaxis=dict(range=[800, -800])),

				clickmode='event+select'
			)
FIG.layout.template = 'plotly_dark_custom'

app.layout = html.Div([
	html.Div(className='icon',
				style={
				'width':'10em'}

			),
	dcc.Graph(id='basic-interactions', figure=
			  FIG, animate=False),
   html.Div(className='row', children=[
		# html.Div([
		# 	dcc.Markdown(),
		# 	html.Pre(id='hover-data', style=styles['pre'])
		# ], className='three columns'),
		html.Div([
			dcc.Markdown(),
			html.Pre(id='click-data', style=styles['pre']),
		], className='two columns')])

	],style={'width': '100%'})


selected_points = []
placeholder = np.zeros_like(Z_DATA)
placeholder[:] = np.nan



area = 0



@app.callback(
	Output('click-data', 'children'),
	[Input('basic-interactions', 'clickData')])
def display_hover_data(clickData):
	message = 'Area : '+str(area)+' mm'
	return json.dumps(message.replace("'", " "), indent=2)





@app.callback(Output('basic-interactions', 'figure'),
			  [Input('basic-interactions', 'clickData')])
def display_click_data(clickData):
	global area
	global selected_points
	global FIG
	if not clickData is None:
		selected_points.append(clickData)
	
	if len(selected_points) == 2:
		x_index = selected_points[0]['points'][0]['x']
		y_index = selected_points[0]['points'][0]['y']
		z_value = selected_points[0]['points'][0]['z']
		x_index_2 = selected_points[1]['points'][0]['x']
		y_index_2 = selected_points[1]['points'][0]['y']
		z_value_2 = selected_points[1]['points'][0]['z']
		

		xmin = x_index if x_index < x_index_2 else x_index_2
		xmax = x_index if x_index > x_index_2 else x_index_2
		ymin = y_index if y_index < y_index_2 else y_index_2
		ymax = y_index if y_index > y_index_2 else y_index_2
		zmin = z_value if z_value < z_value_2 else z_value_2

		area = round((xmax - xmin) * (ymax - ymin)*0.05, 3)


		mid_x = xmin + (xmax-xmin)//2
		mid_y = ymin + (ymax-ymin)//2
		mid_z = zmin + abs(z_value_2 - z_value)

		placeholder[ymin:ymax, xmin:xmax] = Z_DATA[ymin:ymax,xmin:xmax]


		biggest_diff = 0

		for i in range(xmin, xmax):

			first_point = Z_DATA[0, i]
			last_point = Z_DATA[Z_DATA.shape[0]-1, i]
			grad = (last_point - first_point)/(Z_DATA.shape[0]-1 - 0)
			constant = last_point - (grad*(Z_DATA.shape[0]-1))

			for j in range(ymin, ymax):

				predicted_z = grad*j + constant
				real_z = Z_DATA[j, i]
				if np.isnan(real_z):
					continue 

				diff = abs(predicted_z - real_z)

				if diff > biggest_diff:
					biggest_diff = diff

		print("Deepest : ", biggest_diff)




		FIG = go.FigureWidget([go.Surface(z=Z_DATA, opacity=0.8,  colorscale='blues', name='z'),
							   go.Surface(z=placeholder, opacity=0.9, colorscale='Reds')])
		FIG.update_layout(
		height=850,
		clickmode='event+select',
		scene=dict(zaxis=dict(range=[-100, 100]),
					xaxis=dict(range=[800, -800]),

		annotations=[dict(
			x=mid_x,
			y=mid_y,
			z=mid_z,
			text="Area : "+str(area)+" mm",
			textangle=0,
			ax=0,
			ay=-80,
			font=dict(
				color="white",
				size=20
			),
			arrowcolor="white",
			arrowsize=3,
			arrowwidth=2,
			arrowhead=2
				)]
		))
		FIG.layout.template = 'plotly_dark_custom'
		selected_points = []


		return FIG


	raise PreventUpdate #raise an exception in order to prevent any update to the graph








if __name__ == '__main__':
	app.run_server(debug=True)